//
//  ViewController.swift
//  A1Watch
//
//  Created by Mahammad on 2019-10-28.
//  Copyright © 2019 Mahammad. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
       
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
           // Output message to terminal
           print("phone: I received a message: \(message)")
        
        let name = message["name"] as! String
        let age = message["age"] as! String
        namedlabel.text = name
        agedlabel.text = age
    }
     
    
    @IBOutlet weak var namedlabel: UILabel!
    @IBOutlet weak var agedlabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("Phone App loaded")
        
        if (WCSession.isSupported()) == true{
            sendMsg.text = "WCsupported"
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }else{
            sendMsg.text = "WC not Supported"
            
           
        }
}

    @IBOutlet weak var sendMsg: UILabel!
    
    @IBAction func details(_ sender: Any) {
        
        print("sending messege to watch")
        if (WCSession.default.isReachable == true){
            let message = ["fruit":"Banana","colour":"Yellow"] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
            sendMsg.text = "Message Sent"
        }else {
            sendMsg.text = "cannot Reach"
        }
        
    }
    
    
    

}

