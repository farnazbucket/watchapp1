//
//  InterfaceController.swift
//  A1Watch WatchKit Extension
//
//  Created by Mahammad on 2019-10-28.
//  Copyright © 2019 Mahammad. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
           // Output message to terminal
           print("WATCH: I received a message: \(message)")
        
        let fruit = message["fruit"]as! String
        let  colour = message["colour"]as! String
        Fruitlabel.setText(fruit)
        Colourlabel.setText(colour)
        
    }

    @IBOutlet weak var Fruitlabel: WKInterfaceLabel!
    @IBOutlet weak var Colourlabel: WKInterfaceLabel!
    @IBOutlet weak var Mainmessage: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("Watch App loaded")
        
        
        if (WCSession.isSupported() == true) {
                   Mainmessage.setText("WC is supported!")
                   
                   // create a communication session with the phone
                   let session = WCSession.default
                   session.delegate = self
                   session.activate()
               }
               else {
                 Mainmessage.setText("WC NOT supported!")
               }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
//
    @IBAction func personInfo() {
       print("sending messege to phone")
        if (WCSession.default.isReachable == true){
            let message = ["Name":"pritesh","Age":"20"] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
            Mainmessage.setText ("message sent")
        }else {
            Mainmessage.setText ("cannot Reach")
        }

}
}
